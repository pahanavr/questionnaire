export type General = {
  surname: string;
  name: string;
  patronymic: string;
  city: string;
  citizenship: string;
  sex: string;
  bornDate: string;
  birthPlace: string;
}

export type OwnershipIP = {
  inn: number;
  innScan: string;
  registrationDate: string;
  ogrnip: number;
  ogrnipScan: string;
  rentScan: string;
  egripScan: string;
}

export type OwnershipLLC = {
  name: string;
  shortName: string;
  registrationDate: string;
  inn: number;
  innScan: string;
  ogrn: number;
  ogrnScan: string;
}

export type RegistrationAddress = {
  country: string;
  region: string;
  city: string;
  street: string;
  house: number;
  flat: number;
  registrationDate: string;
}

export type HomeAddress = {
  country: string;
  region: string;
  city: string;
  street: string;
  house: number | null;
  flat: number | null;
  registrationDate: string;
}

export type Socials = {
  vk: string | number | null;
  instagram: string | number | null;
  whatsApp: string | number | null;
  youTube: string | number | null;
  ok: string | number | null;
  facebook: string | number | null;
  viber: string | number | null;
  twitter: string | number | null;
  vimeo: string | number | null;
  skype: string | number | null;
}