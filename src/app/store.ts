import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import dataSlice from '../components/features/dataSlice';
import stepSlice from '../components/features/stepSlice';
// import thunk, { ThunkDispatch } from 'redux-thunk';


export const store = configureStore({
  reducer: {
    data: dataSlice,
    step: stepSlice
  },
  devTools: true,
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
