export const regexpInn = /^\d{10}$/;
export const regexpOgrn = /^\d{13}$/;
export const regexpOgrnip = /^\d{15}$/;