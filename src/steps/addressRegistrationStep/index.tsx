import { useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { selectRegistrationAddress, updateRegistrationAddress } from '../../components/features/dataSlice';
import { StepLayout } from '../../components/features/stepLayout';
import { Input } from '../../components/ui/input/Input';
import { Select } from '../../components/ui/select';
import { Errors } from '../../models/error';
import addressIcon from '../../assets/icons/address-icon.svg';
import styles from './styles.module.scss';

const countries = ['Россия', 'Беларусь']
const russianRegions = ['Ленинградская обл.', 'Московская обл.', 'Калининградская обл.']
const belRegions = ['Гомельская обл.', 'Брестская обл.', 'Витебская обл.'];

export const AddressRegistrationStep = ({
  onClickForward,
  onClickBack,
}: {
  onClickForward?: () => void,
  onClickBack?: () => void,
}) => {
  const [isMatchingFlat, setIsMatchingFlat] = useState(false);
  const dispatch = useAppDispatch();
  const registrationAddress = useAppSelector(selectRegistrationAddress)

  const checkRegions = () => {
    if (registrationAddress.country === 'Россия') {
      return russianRegions;
    } else if (registrationAddress.country === 'Беларусь') {
      return belRegions;
    }
    return null;
  }

  // VALIDATION BLOCK
  const [errors, setErrors] = useState<Errors>({
    country: '',
    region: '',
    city: '',
    street: '',
    house: '',
    flat: '',
    registrationDate: '',
  });

  const setError = (name: string, error: string | null) => {
    setErrors(prevErrors => ({ ...prevErrors, [name]: error }));
  };

  const validateInputs = () => {
    const newErrors: Errors = {};
    Object.entries(registrationAddress).forEach(([name, value]) => {
      const error = validateInput(name, value as string);
      if (error) {
        newErrors[name] = error;
      }
    });
    setErrors(newErrors);
  };

  const validateInput = (name: string, value: string) => {
    if (name === 'country' && value === '') {
      return 'Выберите страну';
    }
    if (name === 'region' && value === '') {
      return 'Выберите регион';
    }
    if (name === 'house') {
      if (value.length < 1) {
        return 'Дом';
      }
      if (value.length >= 1) {
        return null;
      }
      return 'Дом';
    }
    if (name === 'flat') {
      if (value.length < 1) {
        return 'Квартира';
      }
      if (value.length >= 1) {
        return null;
      }
      if (isMatchingFlat) {
        return null;
      }
      return 'Квартира';
    }
    if (!value) {
      return 'Обязательное поле';
    }
    return null;
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const field = event.target.name;
    const { name, value } = event.target;
    const error = validateInput(name, value);
    dispatch(updateRegistrationAddress({ field, value: event.target.value }));
    setError(name, error);
  };

  const handleCountrySelect = (selected: string) => {
    const error = validateInput('country', selected);
    setError('country', error);
    dispatch(updateRegistrationAddress({ field: 'country', value: selected }))
  }

  const handleRegionSelect = (selected: string) => {
    const error = validateInput('region', selected);
    setError('region', error);
    dispatch(updateRegistrationAddress({ field: 'region', value: selected }))
  }

  return (
    <StepLayout
      stepIcon={addressIcon}
      title='Адрес регистрации'
      description='Введите свой действуйющий адрес прописки.'
      onClickForward={(e: React.ChangeEvent<HTMLFormControlsCollection>) => {
        e.preventDefault();
        validateInputs();
        if (Object.values(errors).every(error => error === null)) {
          onClickForward && onClickForward();
        }
      }}
      onClickBack={onClickBack}
    >
      <div className={styles.addressRegistration}>
        <Select
          selected={registrationAddress.country}
          label='Страна *'
          items={countries}
          onSelect={handleCountrySelect}
          error={errors.country || ''}
        />
        <Select
          selected={registrationAddress.region}
          label='Регион *'
          items={checkRegions() || []}
          onSelect={handleRegionSelect}
          error={errors.region || ''}
          disabled={registrationAddress.country === ''}
        />
        <Input
          label='Город / Населенный пункт'
          required
          placeholder='Введите населенный пункт'
          name='city'
          onChange={handleInputChange}
          value={registrationAddress.city}
          error={errors.city || ''}
        />
        <Input
          label='Улица'
          required
          placeholder='Введите улицу'
          name='street'
          onChange={handleInputChange}
          value={registrationAddress.street}
          error={errors.street || ''}
        />
        <div className={styles.addressRegistration__homeInfo}>
          <Input
            className={styles.addressRegistration__homeInfoInput}
            label='Дом'
            required
            placeholder='0'
            name='house'
            onChange={handleInputChange}
            value={registrationAddress.house || ''}
            error={errors.house || ''}
          />
          <Input
            className={styles.addressRegistration__homeInfoInput}
            label='Квартира'
            required
            placeholder='0'
            name='flat'
            onChange={handleInputChange}
            value={registrationAddress.flat || ''}
            disabled={isMatchingFlat}
            error={errors.flat || ''}
          />
          <div className={styles.addressRegistration__homeInfoCheckboxContainer}>
            <input
              className={styles.addressRegistration__homeInfoCheckbox}
              type='checkbox'
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                setIsMatchingFlat(!isMatchingFlat)
                const checked = event.target.checked;
                if (checked) {
                  setError('flat', null);
                }
              }}
            />
            <label className={styles.addressRegistration__homeInfoCheckboxLabel}>Нет квартиры</label>
          </div>
        </div>
        <Input
          className={styles.addressRegistration__date}
          label='Дата прописки'
          required
          placeholder='дд.мм.гггг'
          type='date'
          name='registrationDate'
          onChange={handleInputChange}
          value={registrationAddress.registrationDate}
          error={errors.registrationDate || ''}
        />
      </div>
    </StepLayout>
  )
}