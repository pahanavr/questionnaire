import { useState } from 'react';
import { useAppDispatch } from '../../app/hooks';
import { AddInputs } from '../../components/features/addInputs';
import { resetData } from '../../components/features/dataSlice';
import { StepLayout } from '../../components/features/stepLayout';
import { setCurrentStep } from '../../components/features/stepSlice';
import styles from './styles.module.scss';
import socialsIcon from '../../assets/icons/socials-icon.svg';

export const SocailsStep = ({
  onClickForward,
  onClickBack,
}: {
  onClickForward?: () => void,
  onClickBack?: () => void,
}) => {

  const [done, setDone] = useState<boolean>(false)
  const dispatch = useAppDispatch();

  return (
    <>
      {done ?
        <div className={styles.redirect}>
          <span className={styles.redirect__text}>
            Заявка отправлена!
          </span>
          <button onClick={() => {
            dispatch(setCurrentStep(1));
            dispatch(resetData());
          }}
            className={styles.redirect__button}>
            Заполнить еще одну заявку
          </button>
        </div>
        :
        <StepLayout
          stepIcon={socialsIcon}
          title='Социальные сети'
          description='Введите свои действуйющие ссылки на социальные сети и количество подписчиков.'
          onClickForward={() => {
            onClickForward
            setDone(true)
          }}
          onClickBack={onClickBack}
        >
          <div className={styles.socialsStep}>
            <AddInputs />
          </div>
        </StepLayout>}
    </>
  )
}
