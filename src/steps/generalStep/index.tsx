import { useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { StepLayout } from '../../components/features/stepLayout';
import { selectGeneral, updateGeneral } from '../../components/features/dataSlice';
import { Input } from '../../components/ui/input/Input';
import { RadioButtons } from '../../components/ui/radioButtons';
import { Select } from '../../components/ui/select';
import styles from './styles.module.scss';
import generalIcon from '../../assets/icons/general-icon.svg';
import { Errors } from '../../models/error';

const cities = ['Санкт-Петербург', 'Москва', 'Калининград', 'Самара', 'Нижний Новгород']
const citizenships = ['Россия', 'Беларусь', 'Германия', 'Франция']

export const GeneralStep = ({
  onClickForward,
  onClickBack,
}: {
  onClickForward?: () => void,
  onClickBack?: () => void,
}) => {

  const dispatch = useAppDispatch();
  const general = useAppSelector(selectGeneral)

  // VALIDATION BLOCK
  const [errors, setErrors] = useState<Errors>({
    surname: '',
    name: '',
    patronymic: '',
    city: '',
    citizenship: '',
    sex: '',
    birthPlace: '',
    bornDate: '',
  });

  const setError = (name: string, error: string | null) => {
    setErrors(prevErrors => ({ ...prevErrors, [name]: error }));
  };

  const validateInputs = () => {
    const newErrors: Errors = {};
    Object.entries(general).forEach(([name, value]) => {
      const error = validateInput(name, value as string);
      if (error) {
        newErrors[name] = error;
      }
    });
    setErrors(newErrors);
  };

  const validateInput = (name: string, value: string) => {
    if (name === 'city' && value === '') {
      return 'Выберите город';
    }
    if (name === 'citizenship' && value === '') {
      return 'Выберите гражданство';
    }
    if (name === 'sex' && value === '') {
      return 'Выберите пол';
    }
    if (name === 'bornDate' && value === '') {
      return 'Заполните дату рождения';
    }
    if (name === 'birthPlace' && value.trim().length < 10) {
      return 'Место рождения должно содержать как минимум 10 символов';
    }
    if (value.trim() === '') {
      return 'Обязательное поле';
    }
    return null;
  };

  // HANDLERS
  const handleCitySelect = (selected: string) => {
    const error = validateInput('city', selected);
    setError('city', error);
    dispatch(updateGeneral({ field: 'city', value: selected }))
  }

  const handleCitizenshipSelect = (selected: string) => {
    const error = validateInput('citizenship', selected);
    setError('citizenship', error);
    dispatch(updateGeneral({ field: 'citizenship', value: selected }))
  }

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    const error = validateInput('sex', value);
    dispatch(updateGeneral({ field: 'sex', value: value.trim() }));
    setError('sex', error);
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    const error = validateInput(name, value);
    dispatch(updateGeneral({ field: name, value: value.trim() }));
    setError(name, error);
  };

  return (
    <StepLayout
      stepIcon={generalIcon}
      title='Общие'
      description='Введите свои персональные данные.'
      onClickForward={(e: React.ChangeEvent<HTMLFormControlsCollection>) => {
        e.preventDefault();
        validateInputs();
        if (Object.values(errors).every(error => error === null)) {
          onClickForward && onClickForward();
        }
      }}
      onClickBack={onClickBack}
    >
      <div className={styles.generalStep}>
        <Input
          label='Фамилия'
          required={true}
          placeholder='Васильев'
          name='surname'
          onChange={handleInputChange}
          value={general.surname}
          error={errors.surname || ''}
        />
        <Input
          label='Имя'
          required={true}
          placeholder='Иван'
          name='name'
          onChange={handleInputChange}
          value={general.name}
          error={errors.name || ''}
        />
        <Input
          label='Отчество'
          required={true}
          placeholder='Сергеевич'
          name='patronymic'
          onChange={handleInputChange}
          value={general.patronymic}
          error={errors.patronymic || ''}
        />
        <Select
          error={errors.city || ''}
          selected={general.city}
          onSelect={handleCitySelect}
          label='Основной город *'
          items={cities}
        />
        <div className={styles.generalStep__thirdLine}>
          <Select
            error={errors.citizenship || ''}
            selected={general.citizenship}
            onSelect={handleCitizenshipSelect}
            label='Гражданство *'
            items={citizenships}
          />
          <div className={styles.generalStep__thirdLineRightBlock}>
          <RadioButtons error={errors.sex || ''} onChange={handleChange} />
          <Input
            className={styles.generalStep__bornDate}
            label='Дата рождения'
            required={true}
            name='bornDate'
            type='date'
            onChange={handleInputChange}
            value={general.bornDate}
            error={errors.bornDate || ''}
          />
          </div>
        </div>
        <Input
          labelClassName={styles.generalStep__birthPlace}
          label='Место рождения (как указано в паспорте)'
          required={true}
          placeholder='Введите наименование региона и населенного пункта'
          name='birthPlace'
          onChange={handleInputChange}
          value={general.birthPlace}
          error={errors.birthPlace || ''}
        />
      </div>
    </StepLayout>
  )
}