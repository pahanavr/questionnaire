import { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { selectHomeAddress, selectRegistrationAddress, updateHomeAddress } from '../../components/features/dataSlice';
import { StepLayout } from '../../components/features/stepLayout';
import { Input } from '../../components/ui/input/Input';
import { Select } from '../../components/ui/select';
import { Errors } from '../../models/error';
import styles from './styles.module.scss';
import homeAddresIcon from '../../assets/icons/address-icon.svg';

const countries = ['Россия', 'Беларусь']
const russianRegions = ['Ленинградская обл.', 'Московская обл.', 'Калининградская обл.']
const belRegions = ['Гомельская обл.', 'Брестская обл.', 'Витебская обл.'];

export const HomeAddressStep = ({
  onClickForward,
  onClickBack,
}: {
  onClickForward?: () => void,
  onClickBack?: () => void,
}) => {
  const [isMatchingAddress, setIsMatchingAddress] = useState(false);
  const [isMatchingFlat, setIsMatchingFlat] = useState(false);

  const dispatch = useAppDispatch();
  const homeAddress = useAppSelector(selectHomeAddress)
  const registrationAddress = useAppSelector(selectRegistrationAddress)

  const checkRegions = () => {
    if (registrationAddress.country === 'Россия') {
      return russianRegions;
    } else if (registrationAddress.country === 'Беларусь') {
      return belRegions;
    }
    return null;
  }

  // VALIDATION BLOCK
  const [errors, setErrors] = useState<Errors>({
    country: '',
    region: '',
    city: '',
    street: '',
    house: '',
    flat: '',
    registrationDate: '',
  });

  const setError = (name: string, error: string | null) => {
    setErrors(prevErrors => ({ ...prevErrors, [name]: error }));
  };

  useEffect(() => {
    if (isMatchingAddress) {
      setErrors({
        country: null,
        region: null,
        city: null,
        street: null,
        house: null,
        flat: null,
        registrationDate: null,
      });
    } else {
      setErrors({
        country: '',
        region: '',
        city: '',
        street: '',
        house: '',
        flat: '',
        registrationDate: '',
      });
    }
  }, [isMatchingAddress])

  const validateInputs = () => {
    const newErrors: Errors = {};
    Object.entries(homeAddress).forEach(([name, value]) => {
      const error = validateInput(name, value as string);
      if (error) {
        newErrors[name] = error;
      }
    });
    setErrors(newErrors);
  };

  const validateInput = (name: string, value: string) => {
    if (name === 'country' && value === '') {
      return 'Выберите страну';
    }
    if (name === 'region' && value === '') {
      return 'Выберите регион';
    }
    if (name === 'house') {
      if (value.length < 1) {
        return 'Дом';
      }
      if (value.length >= 1) {
        return null;
      }
      return 'Дом';
    }
    if (name === 'flat') {
      if (value.length < 1) {
        return 'Квартира';
      }
      if (value.length >= 1) {
        return null;
      }
      if (isMatchingFlat) {
        return null;
      }
      return 'Квартира';
    }
    if (!value) {
      return 'Обязательное поле';
    }
    return null;
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const field = event.target.name;
    const { name, value } = event.target;
    const error = validateInput(name, value);
    dispatch(updateHomeAddress({ field, value: event.target.value }));
    setError(name, error);
  };

  const handleCountrySelect = (selected: string) => {
    const error = validateInput('country', selected);
    setError('country', error);
    dispatch(updateHomeAddress({ field: 'country', value: selected }))
  }

  const handleRegionSelect = (selected: string) => {
    const error = validateInput('region', selected);
    setError('region', error);
    dispatch(updateHomeAddress({ field: 'region', value: selected }))
  }

  const handleMatchCheckboxChange = () => {
    setIsMatchingAddress(!isMatchingAddress);
  };

  return (
    <StepLayout
      stepIcon={homeAddresIcon}
      title='Адрес проживания'
      description='Введите свой действующий адрес проживания.'
      onClickForward={(e: React.ChangeEvent<HTMLFormControlsCollection>) => {
        e.preventDefault();
        validateInputs();
        if (Object.values(errors).every(error => error === null)) {
          onClickForward && onClickForward();
        }
      }}
      onClickBack={onClickBack}
    >
      <div className={styles.homeAddress}>
        <div className={styles.homeAddress__matchCheckboxContainer}>
          <input
            className={styles.homeAddress__matchCheckbox}
            type='checkbox'
            onChange={handleMatchCheckboxChange}
          />
          <label className={styles.homeAddress__matchCheckboxLabel}>Адрес регистрации и фактического проживания совпадают</label>
        </div>
        <Select
          selected={isMatchingAddress ? registrationAddress.country : homeAddress.country}
          label='Страна *'
          items={countries}
          onSelect={handleCountrySelect}
          disabled={isMatchingAddress}
          error={errors.country || ''}
        />
        <Select
          selected={isMatchingAddress ? registrationAddress.region : homeAddress.region}
          label='Регион *'
          items={checkRegions() || []}
          onSelect={handleRegionSelect}
          disabled={isMatchingAddress}
          error={errors.region || ''}
        />
        <Input
          label='Город / Населенный пункт'
          required={true}
          placeholder='Введите населенный пункт'
          name='city'
          onChange={handleInputChange}
          value={isMatchingAddress ? (registrationAddress.city || '') : (homeAddress.city || '')}
          disabled={isMatchingAddress}
          error={errors.city || ''}
        />
        <Input
          label='Улица'
          required={true}
          placeholder='Введите улицу'
          name='street'
          onChange={handleInputChange}
          value={isMatchingAddress ? (registrationAddress.street || '') : (homeAddress.street || '')}
          disabled={isMatchingAddress}
          error={errors.street || ''}
        />
        <div className={styles.homeAddress__homeInfo}>
          <Input
            className={styles.homeAddress__homeInfoInput}
            label='Дом'
            required={true}
            placeholder='0'
            name='house'
            onChange={handleInputChange}
            value={isMatchingAddress ? (registrationAddress.house || '') : (homeAddress.house || '')}
            disabled={isMatchingAddress}
            error={errors.house || ''}
          />
          <Input
            className={styles.homeAddress__homeInfoInput}
            label='Квартира'
            required={true}
            placeholder='0'
            name='flat'
            onChange={handleInputChange}
            value={isMatchingAddress ? (registrationAddress.flat || '') : (homeAddress.flat || '')}
            disabled={isMatchingAddress || isMatchingFlat}
            error={errors.flat || ''}
          />
          <div className={styles.homeAddress__homeInfoCheckboxContainer}>
            <input
              className={styles.homeAddress__homeInfoCheckbox}
              type='checkbox'
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                setIsMatchingFlat(!isMatchingFlat)
                const checked = event.target.checked;
                if (checked) {
                  setError('flat', null);
                }
              }}
            />
            <label className={styles.homeAddress__homeInfoCheckboxLabel}>Нет квартиры</label>
          </div>
        </div>
        <Input
          className={styles.homeAddress__date}
          label='Дата прописки'
          required={true}
          placeholder='дд.мм.гггг'
          name='registrationDate'
          type='date'
          onChange={handleInputChange}
          value={isMatchingAddress ? (registrationAddress.registrationDate || '') : (homeAddress.registrationDate || '')}
          disabled={isMatchingAddress}
          error={errors.registrationDate || ''}
        />
      </div>
    </StepLayout>
  )
}