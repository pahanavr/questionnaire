import { useEffect, useState } from 'react';
import { StepLayout } from '../../components/features/stepLayout';
import { UploadFile } from '../../components/features/uploadFile';
import { Select } from '../../components/ui/select';
import styles from './styles.module.scss';
import cx from 'classnames';
import ownershipIcon from '../../assets/icons/ownership-icon.svg';
import { Input } from '../../components/ui/input/Input';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { resetOwnershipIp, resetOwnershipLlc, selectOwnershipIP, selectOwnershipLlc, updateOwnerShipIp, updateOwnerShipLlc } from '../../components/features/dataSlice';
import { Errors } from '../../models/error';
import { regexpInn, regexpOgrn, regexpOgrnip } from '../../utils/constants';
import { selectCurrentOwnershipType, setCurrentOwnershipType } from '../../components/features/stepSlice';

const ownerships = ['Индивидуальный предприниматель (ИП)', 'Общество с ограниченной ответственностью (ООО)']

// HARDCODE FOR AUTOCOMPLETE
const info = [
  {
    inn: 1234567890,
    name: 'ООО Газпром',
    shortName: 'ООО Гзпр',
    registrationDate: '2000-01-01',
    ogrn: 123456789012345,
  },
  {
    inn: 1234567891,
    name: 'ООО Московская промышленная компания',
    shortName: 'ООО МПК',
    registrationDate: '2010-05-02',
    ogrn: 123456789012341,
  },
  {
    inn: 1234567892,
    name: 'ООО МТС',
    shortName: 'ООО МТС',
    registrationDate: '2001-10-15',
    ogrn: 123456789012344,
  },
  {
    inn: 1234567893,
    name: 'ПАО АСЗ Янтарь',
    shortName: 'АСЗ Янтарь',
    registrationDate: '1995-03-30',
    ogrn: 123456789012346,
  }
]

export const OwnershipStep = ({
  onClickForward,
  onClickBack,
}: {
  onClickForward?: () => void,
  onClickBack?: () => void,
}) => {
  const [isMatchingContract, setIsMatchingContract] = useState(false);
  const ownershipIp = useAppSelector(selectOwnershipIP);
  const ownershipLlc = useAppSelector(selectOwnershipLlc);
  const currentOwnershipType = useAppSelector(selectCurrentOwnershipType);

  const dispatch = useAppDispatch()

  const [errors, setErrors] = useState<Errors>({
    inn: '',
    innScan: '',
    registrationDate: '',
    ogrnip: '',
    ogrnipScan: '',
    rentScan: null,
    egripScan: '',
  });

  const [llcErrors, setLlcErrors] = useState<Errors>({
    name: '',
    shortName: '',
    registrationDate: '',
    inn: '',
    innScan: '',
    ogrn: '',
    ogrnScan: '',
  });

  // SET IP ERRORS
  const setIpError = (name: string, error: string | null) => {
    setErrors(prevErrors => ({ ...prevErrors, [name]: error }));
  };

  // SET LLC ERRORS
  const setLlcError = (name: string, error: string | null) => {
    setLlcErrors(prevErrors => ({ ...prevErrors, [name]: error }));
  };

  const validateIpInputs = () => {
    const newErrors: Errors = {};
    Object.entries(ownershipIp).forEach(([name, value]) => {
      const error = validateIpInput(name, value as string);
      if (error) {
        newErrors[name] = error;
      }
    });
    setErrors(newErrors);
  };

  const validateLlcInputs = () => {
    const newErrors: Errors = {};
    Object.entries(ownershipLlc).forEach(([name, value]) => {
      const error = validateLlcInput(name, value as string);
      if (error) {
        newErrors[name] = error;
      }
    });
    setLlcErrors(newErrors);
  };

  // VALIDATE IP INPUTS
  const validateIpInput = (name: string, value: string) => {
    if (name === 'inn') {
      if (!value) {
        return 'Обязательное поле';
      }
      if (value.length !== 10) {
        return 'Только 10 цифр';
      }
      if (!regexpInn.test(value)) {
        return 'Только цифры';
      }
    }
    if (name === 'ogrnip') {
      if (!value) {
        return 'Обязательное поле';
      }
      if (value.length !== 15) {
        return 'Только 15 цифр';
      }
      if (!regexpOgrnip.test(value)) {
        return 'Только цифры';
      }
    }
    if (name === 'rentScan') {
      return null;
    }
    if (!value) {
      return 'Обязательное поле';
    }
    return null;
  };

  // VALIDATE LLC INPUTS
  const validateLlcInput = (name: string, value: string) => {
    if (name === 'inn') {
      if (!value) {
        return 'Обязательное поле';
      }
      if (value.length !== 10) {
        return 'Только 10 цифр';
      }
      if (!regexpInn.test(value)) {
        return 'Только цифры';
      }
    }
    if (name === 'ogrn') {
      if (!value) {
        return 'Обязательное поле';
      }
      if (value.length !== 13) {
        return 'Только 10 цифр';
      }
      if (!regexpOgrn.test(value)) {
        return 'Только цифры';
      }
    }
    if (!value) {
      return 'Обязательное поле';
    }
    return null;
  };

  // MAIN SELECT HANDLER
  const handleSelect = (selected: string) => {
    dispatch(setCurrentOwnershipType(selected))
    if (selected === 'Индивидуальный предприниматель (ИП)') {
      dispatch(resetOwnershipLlc())
    }
    if (selected === 'Общество с ограниченной ответственностью (ООО)') {
      dispatch(resetOwnershipIp())
    }
  }

  // INPUT HANDLER
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const field = event.target.name;
    if (currentOwnershipType === 'Индивидуальный предприниматель (ИП)') {
      const { name, value } = event.target;
      const error = validateIpInput(name, value);
      dispatch(updateOwnerShipIp({ field, value: event.target.value }));
      setIpError(name, error);
    } else {
      const { name, value } = event.target;
      const error = validateLlcInput(name, value);
      dispatch(updateOwnerShipLlc({ field, value: event.target.value }));
      setLlcError(name, error);
      if (name === 'inn') {
        handleInnChange(event);
      }
    }
  };

  // UPLOAD FILE HANDLER
  const handleUploadChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const field = event.target.name;
    const { name, value } = event.target;
    const file = event.target.files && event.target.files[0];
    if (currentOwnershipType === 'Индивидуальный предприниматель (ИП)') {
      const error = validateIpInput(name, value);
      dispatch(updateOwnerShipIp({ field, value: file ? file.name : '' }));
      setIpError(name, error);
    }
    else {
      const error = validateLlcInput(name, value);
      dispatch(updateOwnerShipLlc({ field, value: file ? file.name : '' }));
      setLlcError(name, error);
    }
  };

  // AUTOCOMPLETE FUNCTION
  const handleInnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const inn = event.target.value;
    const foundCompany = info.find(company => company.inn === Number(inn));
    if (foundCompany) {
      dispatch(updateOwnerShipLlc({
        field: 'name',
        value: foundCompany.name
      }));
      dispatch(updateOwnerShipLlc({
        field: 'shortName',
        value: foundCompany.shortName
      }));
      dispatch(updateOwnerShipLlc({
        field: 'registrationDate',
        value: foundCompany.registrationDate
      }));
      dispatch(updateOwnerShipLlc({
        field: 'ogrn',
        value: foundCompany.ogrn
      }));
    } else {
      dispatch(updateOwnerShipLlc({
        field: 'name',
        value: ''
      }));
      dispatch(updateOwnerShipLlc({
        field: 'shortName',
        value: ''
      }));
      dispatch(updateOwnerShipLlc({
        field: 'registrationDate',
        value: ''
      }));
      dispatch(updateOwnerShipLlc({
        field: 'ogrn',
        value: ''
      }));
    }
  };

  // RESET ERRORS
  useEffect(() => {
    if (currentOwnershipType === 'Индивидуальный предприниматель (ИП)') {
      setLlcErrors({
        name: '',
        shortName: '',
        registrationDate: '',
        inn: '',
        innScan: '',
        ogrn: '',
        ogrnScan: '',
      });
    } else {
      setErrors({
        inn: '',
        innScan: '',
        registrationDate: '',
        ogrnip: '',
        ogrnipScan: '',
        rentScan: null,
        egripScan: '',
      });
    }
  }, [currentOwnershipType])

  return (
    <StepLayout
      stepIcon={ownershipIcon}
      title='Форма собственности'
      description='Выберите форму собственности и заполните данные'
      onClickForward={(e: React.ChangeEvent<HTMLFormControlsCollection>) => {
        e.preventDefault();
        if (currentOwnershipType === 'Индивидуальный предприниматель (ИП)') {
          validateIpInputs();
        }
        if (currentOwnershipType === 'Общество с ограниченной ответственностью (ООО)') {
          validateLlcInputs();
        }
        if (Object.values(errors).every(error => error === null)) {
          onClickForward && onClickForward();
        }
      }}
      onClickBack={onClickBack}
      disabled={currentOwnershipType === ''}
    >
      <Select
        selected={currentOwnershipType}
        onSelect={handleSelect}
        className={cx(styles.ownershipStep__select, currentOwnershipType !== '' && styles.ownershipIp__select_type_active)}
        label='Вид деятельности'
        items={ownerships}
      />
      {(currentOwnershipType === 'Индивидуальный предприниматель (ИП)') &&
        <div className={cx(styles.ownershipStep__main, styles.ownershipStep__main_type_individual)}>
          <div className={styles.ownershipStep__firstLine}>
            <Input
              label='ИНН'
              required
              name='inn'
              value={ownershipIp.inn || ''}
              onChange={handleInputChange}
              placeholder='xxxxxxxxxx'
              labelClassName={cx(styles.ownershipStep__input, styles.ownershipStep__input_type_short)}
              error={errors.inn || ''}
            />
            <UploadFile
              onChange={handleUploadChange}
              className={styles.ownershipStep__uploadInput}
              name='innScan'
              label='Скан ИНН'
              required
              fileName={ownershipIp.innScan || ''}
              error={errors.innScan || ''}
            />
            <Input
              label='Дата регистрации'
              name='registrationDate'
              value={ownershipIp.registrationDate}
              onChange={handleInputChange}
              required
              type='date'
              placeholder='дд.мм.гггг'
              labelClassName={cx(styles.ownershipStep__input, styles.ownershipStep__input_type_short)}
              error={errors.registrationDate || ''}
            />
          </div>
          <div className={styles.ownershipStep__secondLine}>
            <Input
              label='ОГРНИП'
              name='ogrnip'
              value={ownershipIp.ogrnip || ''}
              onChange={handleInputChange}
              required
              placeholder='xxxxxxxxxxxxxxx'
              labelClassName={cx(styles.ownershipStep__input, styles.ownershipStep__input_type_long)}
              error={errors.ogrnip || ''}
            />
            <UploadFile
              onChange={handleUploadChange}
              name='ogrnipScan'
              label='Скан ОГРНИП'
              required
              fileName={ownershipIp.ogrnipScan || ''}
              error={errors.ogrnipScan || ''}
            />
          </div>
          <div className={styles.ownershipStep__thirdLine}>
            <div className={styles.ownershipStep__checkboxBlock}>
              <UploadFile
                onChange={handleUploadChange}
                name='rentScan'
                label='Скан договора аренды помещения (офиса)'
                disabled={isMatchingContract}
                fileName={ownershipIp.rentScan || ''}
              />
              <div className={styles.ownershipStep__checkboxContainer}>
                <input
                  className={styles.ownershipStep__checkbox}
                  type='checkbox'
                  onChange={() => setIsMatchingContract(!isMatchingContract)}
                />
                <label className={styles.ownershipStep__checkboxLabel}>Нет договора</label>
              </div>
            </div>
            <UploadFile
              onChange={handleUploadChange}
              name='egripScan'
              label='Скан выписки из ЕГРИП (не старше 3 месяцев)'
              required
              fileName={ownershipIp.egripScan || ''}
              error={errors.egripScan || ''}
            />
          </div>
        </div>}
      {(currentOwnershipType === 'Общество с ограниченной ответственностью (ООО)') &&
        <div className={cx(styles.ownershipStep__main, styles.ownershipStep__main_type_llc)}>
          <div className={cx(styles.ownershipStep__firstLine, styles.ownershipStep__firstLine_type_llc)}>
            <Input
              label='Наименование полное'
              required
              name='name'
              value={ownershipLlc.name}
              onChange={handleInputChange}
              placeholder='ООО «Московская промышленная компания»'
              labelClassName={cx(styles.ownershipStep__input, styles.ownershipStep__input_type_name)}
              error={llcErrors.name || ''}
            />
            <Input
              label='Сокращение'
              required={true}
              name='shortName'
              value={ownershipLlc.shortName}
              onChange={handleInputChange}
              placeholder='ООО "МПК"'
              labelClassName={cx(styles.ownershipStep__input, styles.ownershipStep__input_type_short)}
              error={llcErrors.shortName || ''}
            />
          </div>
          <div className={cx(styles.ownershipStep__secondLine, styles.ownershipStep__secondLine_type_llc)}>
            <Input
              label='Дата регистрации'
              required
              name='registrationDate'
              value={ownershipLlc.registrationDate || ''}
              onChange={handleInputChange}
              placeholder='дд.мм.гггг'
              type='date'
              labelClassName={cx(styles.ownershipStep__input, styles.ownershipStep__input_type_short)}
              error={llcErrors.registrationDate || ''}
            />
            <Input
              label='ИНН'
              required
              name='inn'
              value={ownershipLlc.inn || ''}
              onChange={handleInputChange}
              placeholder='xxxxxxxxxx'
              labelClassName={cx(styles.ownershipStep__input, styles.ownershipStep__input_type_short)}
              error={llcErrors.inn || ''}
            />
            <UploadFile
              error={llcErrors.innScan || ''}
              onChange={handleUploadChange}
              required
              name='innScan'
              label='Скан ИНН'
              fileName={ownershipLlc.innScan || ''}
            />
          </div>
          <div className={cx(styles.ownershipStep__thirdLine, styles.ownershipStep__thirdLine_type_llc)}>
            <Input
              label='ОГРН'
              required
              name='ogrn'
              value={ownershipLlc.ogrn || ''}
              onChange={handleInputChange}
              placeholder='xxxxxxxxxxxxx'
              labelClassName={cx(styles.ownershipStep__input, styles.ownershipStep__input_type_short)}
              error={llcErrors.ogrn || ''}
            />
            <UploadFile
              error={llcErrors.ogrnScan || ''}
              onChange={handleUploadChange}
              required
              className={cx(styles.ownershipStep__uploadInput)}
              name='ogrnScan'
              fileName={ownershipLlc.ogrnScan || ''}
              label='Скан ОГРН'
            />
          </div>
        </div>}
    </StepLayout>
  )
}
