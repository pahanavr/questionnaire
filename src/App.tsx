import { useDispatch } from 'react-redux';
import { useAppSelector } from './app/hooks';
import { selectSubmitJSON } from './components/features/dataSlice';
import { PageLayout } from './components/features/pageLayout'
import { selectCurrentStep, setNextStep, setPreviousStep } from './components/features/stepSlice';
import { AddressRegistrationStep } from './steps/addressRegistrationStep';
import { GeneralStep } from './steps/generalStep'
import { HomeAddressStep } from './steps/homeAddressStep';
import { OwnershipStep } from './steps/ownershipStep';
import { SocailsStep } from './steps/socialsStep';

function App() {
  const dispatch = useDispatch();
  const currentStep = useAppSelector(selectCurrentStep)

  const handleBackClick = () => {
    dispatch(setPreviousStep());
  };

  const handleNextClick = () => {
    dispatch(setNextStep());
  };

  const data = useAppSelector(selectSubmitJSON)
  console.log(data)

  return (
    <PageLayout>
      {currentStep === 1 && <GeneralStep onClickForward={handleNextClick} onClickBack={handleBackClick} />}
      {currentStep === 2 && <OwnershipStep onClickForward={handleNextClick} onClickBack={handleBackClick} />}
      {currentStep === 3 && <AddressRegistrationStep onClickForward={handleNextClick} onClickBack={handleBackClick} />}
      {currentStep === 4 && <HomeAddressStep onClickForward={handleNextClick} onClickBack={handleBackClick} />}
      {currentStep === 5 && <SocailsStep onClickForward={handleNextClick} onClickBack={handleBackClick} />}
    </PageLayout>
  )
}

export default App;
