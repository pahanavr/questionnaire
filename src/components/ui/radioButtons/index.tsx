import { useAppSelector } from '../../../app/hooks';
import { selectGeneral } from '../../features/dataSlice';
import manIcon from '../../../assets/icons/man-icon.svg';
import manIconActive from '../../../assets/icons/man-icon-active.svg';
import womanIcon from '../../../assets/icons/woman-icon.svg';
import womanIconActive from '../../../assets/icons/woman-icon-active.svg';
import styles from './styles.module.scss';

export const RadioButtons = ({
  onChange,
  error
}: {
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void,
  error?: string
}) => {
  const general = useAppSelector(selectGeneral)

  return (
    <div className={styles.radioButtons}>
      <span className={styles.radioButtons__label}>
        Пол*
      </span>
      <div className={styles.radioButtons__container}>
        <div className={styles.radioButtons__buttonContainer}>
          <input
            className={styles.radioButtons__input}
            id='man'
            type='radio'
            name='sex'
            value='Мужской'
            onChange={onChange}
            required={true}
            checked={general.sex === 'Мужской'}
          />
          <label htmlFor='man' className={styles.radioButtons__button}>
            <img src={general.sex !== 'Мужской' ? manIcon : manIconActive} alt='man-icon' />
            <span className={styles.radioButtons__text}>М</span>
          </label>
        </div>
        <div className={styles.radioButtons__buttonContainer}>
          <input
            className={styles.radioButtons__input}
            id='woman'
            type='radio'
            name='sex'
            value='Женский'
            onChange={onChange}
            required={true}
            checked={general.sex === 'Женский'}
          />
          <label htmlFor='woman' className={styles.radioButtons__button}>
            <img src={general.sex !== 'Женский' ? womanIcon : womanIconActive} alt='woman-icon' />
            <span className={styles.radioButtons__text}>Ж</span>
          </label>
        </div>
      </div>
      {error && <span className={styles.radioButtons__error}>{error}</span>}
    </div>
  )
}