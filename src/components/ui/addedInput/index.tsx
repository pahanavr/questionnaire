import { ChangeEvent, useState } from 'react';
import { Input } from '../input/Input';
import { Select } from '../select';
import deleteIcon from '../../../assets/icons/delete-icon.svg';
import styles from './styles.module.scss';

import cx from 'classnames';
import { useAppDispatch } from '../../../app/hooks';
import { updateSocials } from '../../features/dataSlice';

type Props = {
  id?: string;
  name: string;
  label?: string;
  labelClassName?: string;
  placeholder?: string;
  onRemove?: any;
  value?: string;
  onChange: (name: string, value: string) => void;
  items?: any;
  selected?: string;
}

export const AddedInput = ({
  id,
  name,
  label,
  labelClassName,
  placeholder,
  onRemove,
  onChange,
  items,
  selected: initialSelected,
}: Props) => {

  const [selectedValue, setSelectedValue] = useState('');
  const [selected, setSelected] = useState(initialSelected);
  const [fieldName, setFieldName] = useState(mapSelectedValueToFieldName(initialSelected!));

  const dispatch = useAppDispatch();

  function mapSelectedValueToFieldName(value: string): string {
    switch (value) {
      case 'Вконтакте':
        return 'vk';
      case 'Instagram':
        return 'instagram';
      case 'WhatsApp':
        return 'whatsApp';
      case 'YouTube':
        return 'youtube';
      case 'Одноклассники':
        return 'ok';
      case 'Facebook':
        return 'facebook';
      case 'Viber':
        return 'viber';
      case 'Twitter':
        return 'twitter';
      case 'Vimeo':
        return 'vimeo';
      case 'Skype':
        return 'skype';
      default:
        return '';
    }
  }

  const handleSelectChange = (value: string) => {
    setSelected(value);

    const newFieldName = mapSelectedValueToFieldName(value);

    setFieldName(newFieldName);
    onChange(name, '');

    if (newFieldName) {
      setSelectedValue('');
      onChange(mapSelectedValueToFieldName(selected!), '');
    }
  };

  return (
    <div key={id} className={styles.addedInput}>
      <Select
        label='Сайт / Приложение'
        className={cx(styles.addedInput__select, styles.addedInput__select_type_added)}
        items={items}
        onSelect={handleSelectChange}
        selected={selected}
      />
      <Input
        className={styles.addedInput__input}
        name={mapSelectedValueToFieldName(selected!)}
        label={label}
        labelClassName={labelClassName}
        placeholder={placeholder}
        value={selectedValue || ''}
        onChange={(e: ChangeEvent<HTMLInputElement>) => {
          dispatch(updateSocials({ field: fieldName, value: e.target.value }));
          setSelectedValue(e.target.value);
          onChange(fieldName, e.target.value);
        }}
      />
      <button type='button'
        className={styles.addedInput__deleteButton}
        onClick={() => {
          onRemove && onRemove()
          const fieldName = mapSelectedValueToFieldName(name);
          dispatch(updateSocials({ field: fieldName, value: '' }))
        }
        }>
        <img src={deleteIcon} alt='delete-icon' />
      </button>
    </div>
  )
}