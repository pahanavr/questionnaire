import { MutableRefObject, useEffect, useRef, useState } from 'react';
import styles from './styles.module.scss';
import cx from 'classnames';
import downArrowIcon from '../../../assets/icons/down-arrow.svg';

type Props = {
  items: string[];
  className?: string;
  label?: string;
  onSelect?: (selected: string) => void;
  selected?: string;
  disabled?: boolean;
  error?: string;
}

export const Select = ({
  items,
  className,
  label,
  onSelect,
  selected,
  disabled,
  error,
}: Props) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const root = useRef(null) as MutableRefObject<any>;

  const handleOpenSelect = () => {
    setIsOpen(!isOpen)
    if (disabled) {
      setIsOpen(false)
    }
  }

  const handleSelect = (item: string) => {
    setIsOpen(false);
    onSelect && onSelect(item);
  }

  useEffect(() => {
    const outsideClick = (e: MouseEvent) => root.current.contains(e.target) || setIsOpen(false);
    document.addEventListener('click', outsideClick);
    return () => document.removeEventListener('click', outsideClick);
  }, [isOpen]);

  return (
    <div ref={root} className={cx(styles.select, className, disabled && styles.select_type_disabled)}>
      <span className={styles.select__label}>{label}</span>
      <div onClick={handleOpenSelect} className={cx(styles.select__titleContainer, isOpen && styles.select__titleContainer_without_radius, disabled && styles.select__titleContainer_type_disabled)}>
        <p className={styles.select__title}>{selected || 'Выбрать'}</p>
        <img className={cx(styles.select__titleIcon, isOpen && styles.select__titleIcon_type_rotate)} src={downArrowIcon} alt='down-arrow' />
        {error && <span className={styles.select__error}>{error}</span>}
      </div>
      {isOpen && <ul className={styles.select__list}>
        {items.map((item, index) => (
          <li
            key={index}
            onClick={() => handleSelect(item)}
            className={styles.select__listItem}
          >
            <p className={styles.select__listItemText}>{item}</p>
          </li>
        ))}
      </ul>}
    </div>
  )
}