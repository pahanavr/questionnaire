import styles from './styles.module.scss';
import subIcon from '../../../assets/icons/sub-icon.svg';

export const StepIcon = ({ image }: { image: string }) => {
  return (
    <div className={styles.stepIcon}>
      <img src={image} alt='step-icon' />
      <img className={styles.stepIcon__subImage} src={subIcon} alt='sub-icon' />
    </div>
  )
}