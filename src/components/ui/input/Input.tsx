import styles from './styles.module.scss';
import cx from 'classnames';
import { useState } from 'react';

type Props = {
  type?: 'text' | 'number' | 'email' | 'password' | 'search' | 'date';
  name?: string;
  placeholder?: string;
  value?: string | number;
  label?: string;
  className?: string;
  labelClassName?: string;
  required?: boolean;
  children?: string | JSX.Element | JSX.Element[];
  defaultChecked?: boolean;
  onChange?: any;
  defaultValue?: string;
  disabled?: boolean;
  error?: string;
}

export const Input = ({
  type = 'text',
  name,
  placeholder,
  value,
  label,
  className,
  labelClassName,
  required = false,
  children,
  defaultChecked,
  onChange,
  defaultValue,
  disabled,
  error,
}: Props) => {

  return (
    <label className={cx(styles.inputComponent, labelClassName)}>
      <span className={styles.inputComponent__label}>
        {label} {required && '*'}
      </span>
      <input
        className={cx(styles.inputComponent__input, className, disabled && styles.inputComponent__input_type_disabled)}
        type={type}
        name={name}
        placeholder={placeholder}
        value={value}
        required={required}
        defaultChecked={defaultChecked}
        onChange={onChange}
        defaultValue={defaultValue}
        disabled={disabled}
      />   
      {error && <span className={styles.inputComponent__error}>{error}</span>}
      {children}
    </label>
  )
}
