import { useState } from 'react'
import { AddedInput } from '../../ui/addedInput';
import { Select } from '../../ui/select';
import cx from 'classnames';
import addIcon from '../../../assets/icons/add-icon.svg';

import styles from './styles.module.scss';
import { useAppDispatch } from '../../../app/hooks';
import { updateSocials } from '../dataSlice';

const socials = ['Вконтакте', 'Instagram', 'WhatsApp', 'YouTube', 'Одноклассники', 'Facebook', 'Viber', 'Twitter', 'Vimeo', 'Skype'];

export const AddInputs = () => {
  const [items, setItems] = useState<string[]>([]);
  const [selected, setSelected] = useState('Выбрать');
  const [hideSelect, setHideSelect] = useState<boolean>(false);

  const dispatch = useAppDispatch();

  const handleDelete = (name: string) => {
    const newItems = [...items.filter(item => item !== name)]
    const firstPick = socials.find(option =>
      !newItems.includes(option)
    );

    setItems(newItems);
    setSelected(firstPick ? firstPick : '');

    if (!firstPick) setHideSelect(true)
    else setHideSelect(false)
  }

  const handleChange = (value: string) => {
    setSelected(value);
  }

  const handleAdd = () => {
    const newItems = [...items, selected];
    const firstPick = socials.find((option) =>
      !newItems.includes(option)
    );

    setItems(newItems);
    setSelected(firstPick ? firstPick : '');

    if (!firstPick) {
      setHideSelect(true)
    } else {
      setHideSelect(false)
    }
  }

  const handleInputChange = (name: string, value: string) => {
    if (name === 'Вконтакте') {
      dispatch(updateSocials({ field: 'vk', value: value }))
    } else if (name === 'Instagram') {
      dispatch(updateSocials({ field: 'instagram', value: value }))
    } else if (name === 'WhatsApp') {
      dispatch(updateSocials({ field: 'whatsApp', value: value }))
    } else if (name === 'YouTube') {
      dispatch(updateSocials({ field: 'youtube', value: value }))
    } else if (name === 'Одноклассники') {
      dispatch(updateSocials({ field: 'ok', value: value }))
    } else if (name === 'Facebook') {
      dispatch(updateSocials({ field: 'facebook', value: value }))
    } else if (name === 'Viber') {
      dispatch(updateSocials({ field: 'viber', value: value }))
    } else if (name === 'Twitter') {
      dispatch(updateSocials({ field: 'twitter', value: value }))
    } else if (name === 'Vimeo') {
      dispatch(updateSocials({ field: 'vimeo', value: value }))
    } else if (name === 'Skype') {
      dispatch(updateSocials({ field: 'skype', value: value }))
    }
  }

  const filteredSocials = socials.filter((item) => !items.includes(item));

  return (
    <div className={styles.addInputs}>
      <div className={cx(styles.addInputs__selectContainer, items.length > 0 && styles.addInputs__selectContainer_type_disabled)}>
        <Select
          label='Сайт / Приложение'
          className={styles.addInputs__select}
          items={socials}
          onSelect={handleChange}
          selected={selected}
        />
      </div>
      {items.map((item, index) => (
        <div key={index} className={styles.addInputs__addedInput}>
          <AddedInput
            name={item}
            labelClassName={styles.addInputs__input}
            onRemove={() => handleDelete(item)}
            onChange={handleInputChange}
            selected={item}
            items={filteredSocials}
          />
        </div>
      ))}
      <button
        className={styles.addInputs__addButton}
        onClick={handleAdd}
        type='button'
        disabled={selected === '' || selected === 'Выбрать'}
      >
        <img className={styles.addInputs__addButtonIcon} src={addIcon} alt='add-icon' />
        <p className={styles.addInputs__addButtonText}>Добавить социальную сеть</p>
      </button>
    </div>
  )
}
