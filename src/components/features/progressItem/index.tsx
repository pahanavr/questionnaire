import { useAppSelector } from '../../../app/hooks';
import { selectCurrentStep } from '../stepSlice';
import stepIcon from '../../../assets/icons/doneStep-icon.svg';
import styles from './styles.module.scss';
import cx from 'classnames';

type Props = {
  step: number;
  text: string;
}

export const ProgressItem = ({
  step,
  text,
}: Props) => {
  const currentStep = useAppSelector(selectCurrentStep)
  const checkPageIsDone = step < currentStep;
  const checkCurrentPage = step === currentStep;

  return (
    <div className={styles.progressItem}>
      <div
        className={cx(styles.progressItem__iconContainer,
          checkPageIsDone && styles.progressItem__iconContainer_type_done,
          checkCurrentPage && styles.progressItem__iconContainer_type_current
        )}>
        <span
          className={cx(styles.progressItem__iconText,
            (checkPageIsDone || checkCurrentPage) && styles.progressItem__iconText_type_done,
          )}>
          {step}
        </span>
      </div>
      <p className={cx(styles.progressItem__text, checkCurrentPage && styles.progressItem__text_type_current)}>
        {text}
      </p>
      {checkPageIsDone && <img src={stepIcon} alt='doneStep-icon' />}
    </div>
  )
}
