import styles from './styles.module.scss';
import cx from 'classnames';
import { updateOwnerShipIp, updateOwnerShipLlc } from '../dataSlice';
import { useAppDispatch, useAppSelector } from '../../../app/hooks';
import { selectCurrentOwnershipType } from '../stepSlice';
import uploadIcon from '../../../assets/icons/upload-icon.svg';
import doneIcon from '../../../assets/icons/done-icon.svg';
import deleteIcon from '../../../assets/icons/delete-icon.svg';


type Props = {
  name: string;
  label: string;
  className?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>, name: string) => void;
  required?: boolean;
  disabled?: boolean;
  error?: string;
  value?: string;
  fileName?: string;
}

export const UploadFile = ({
  name,
  label,
  className,
  onChange,
  required,
  disabled,
  error,
  value,
  fileName,
}: Props) => {
  const dispatch = useAppDispatch();
  const currentOwnershipType = useAppSelector(selectCurrentOwnershipType);

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    const fileName = file?.name || '';
    onChange && onChange(event, fileName);
  };

  const handleDeleteFile = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    if (currentOwnershipType === 'Индивидуальный предприниматель (ИП)') {
      dispatch(updateOwnerShipIp({ field: name, value: '' }));
    } else {
      dispatch(updateOwnerShipLlc({ field: name, value: '' }));
    }
  }

  return (
    <div className={cx(styles.uploadFile, disabled && styles.uploadFile_type_disabled)}>
      <label htmlFor={name} className={styles.uploadFile__labelName}>
        {label} {required && '*'}
      </label>
      <label className={styles.uploadFile__label}>
        <span className={cx(styles.uploadFile__inputFileText, className, fileName && styles.uploadFile__inputFileText_type_radius)}>
          {fileName ?
            <>
              <img className={styles.uploadFile__doneIcon} src={doneIcon} alt='done-icon' />
              <span className={styles.uploadFile__text}>{fileName}</span>
            </>
            :
            'Выберите или перетащите файл'
          }
        </span>
        <input
          className={styles.uploadFile__input}
          type="file"
          name={name}
          id={name}
          required={required}
          onChange={handleFileChange}
          disabled={disabled}
          value={value}
        />
        {!fileName ? <span className={styles.uploadFile__inputFileButton}>
          <img
            className={styles.uploadFile__inputFileIcon}
            src={uploadIcon}
            alt='upload-icon'
          />
        </span>
          :
          <button className={styles.uploadFile__deleteButton} onClick={handleDeleteFile}>
            <img src={deleteIcon} alt='delete-icon' />
          </button>
        }
      </label>
      {error && <span className={styles.uploadFile__error}>{error}</span>}
    </div>
  );
};
