import { StepIcon } from '../../ui/stepIcon';
import styles from './styles.module.scss';
import cx from 'classnames';
import { useAppSelector } from '../../../app/hooks';
import { selectCurrentStep } from '../stepSlice';

type Props = {
  children?: JSX.Element | JSX.Element[] | React.ReactNode;
  stepIcon: string;
  title: string;
  description: string;
  className?: string;
  onClickForward?: (e?: any) => void;
  onClickBack?: () => void;
  disabled?: boolean;
}

export const StepLayout = ({
  children,
  stepIcon,
  title,
  description,
  className,
  onClickForward,
  onClickBack,
  disabled,
}: Props) => {
  const currentStep = useAppSelector(selectCurrentStep)
  return (
    <form noValidate onSubmit={onClickForward} className={cx(styles.stepLayout, className)}>
      <div className={styles.stepLayout__titleContainer}>
        <StepIcon image={stepIcon} />
        <h2 className={styles.stepLayout__title}>
          {title}
        </h2>
        <p className={styles.stepLayout__description}>
          {description}
        </p>
      </div>
      {children}
      <div className={styles.stepLayout__buttonsContainer}>
        <button disabled={currentStep === 1} onClick={onClickBack} className={styles.stepLayout__backButton}>
          {currentStep === 1 ? 'Отмена' : 'Назад'}
        </button>
        {currentStep === 5 ?
          <button
            disabled={disabled}
            type='submit'
            className={cx(styles.stepLayout__forwardButton, disabled && styles.stepLayout__forwardButton_type_disabled)}>
            Сохранить
          </button>
          :
          <button
            disabled={disabled}
            type='submit'
            className={cx(styles.stepLayout__forwardButton, disabled && styles.stepLayout__forwardButton_type_disabled)}>
            Далее
          </button>}
      </div>
    </form>
  )
}