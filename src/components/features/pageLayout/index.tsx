import styles from './styles.module.scss';
import cx from 'classnames';
import { ProgressFill } from '../progressFill';

type Props = {
  children?: React.ReactNode;
  className?: string;
}

export const PageLayout = ({
  children,
  className,
}: Props) => {
  return (
    <div className={cx(styles.page, className)}>
      <ProgressFill />
      {children}
    </div>
  )
}