import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../app/store';

export interface StepState {
  currentStep: number;
  currentOwnershipType: string;
}

export const initialState: StepState = {
  currentStep: 1,
  currentOwnershipType: '',
}

export const stepSlice = createSlice({
  name: 'step',
  initialState,
  reducers: {
    setCurrentStep: (state, action: PayloadAction<number>) => {
      state.currentStep = action.payload;
    },
    setNextStep(state) {
      state.currentStep += 1;
    },
    setPreviousStep(state) {
      state.currentStep -= 1;
    },
    setCurrentOwnershipType: (state, action: PayloadAction<string>) => {
      state.currentOwnershipType = action.payload;
    },
  }
})

export const {
  setCurrentStep,
  setNextStep,
  setPreviousStep,
  setCurrentOwnershipType,
} = stepSlice.actions

export const selectCurrentStep = (state: RootState) => state.step.currentStep;
export const selectCurrentOwnershipType = (state: RootState) => state.step.currentOwnershipType;

export default stepSlice.reducer;