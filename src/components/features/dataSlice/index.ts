import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../../app/store";
import { General, HomeAddress, OwnershipIP, OwnershipLLC, RegistrationAddress, Socials } from "../../../models/stepsModels";

export interface DataState {
  general: General;
  ownerShipIp: OwnershipIP;
  ownerShipLlc: OwnershipLLC;
  registrationAddress: RegistrationAddress;
  homeAddress: HomeAddress;
  socials: Socials;
}

export const initialState: DataState = {
  general: {
    surname: '',
    name: '',
    patronymic: '',
    city: '',
    citizenship: '',
    sex: '',
    bornDate: '',
    birthPlace: '',
  },
  ownerShipIp: {
    inn: 0,
    innScan: '',
    registrationDate: '',
    ogrnip: 0,
    ogrnipScan: '',
    rentScan: '',
    egripScan: '',
  },
  ownerShipLlc: {
    name: '',
    shortName: '',
    registrationDate: '',
    inn: 0,
    innScan: '',
    ogrn: 0,
    ogrnScan: '',
  },
  registrationAddress: {
    country: '',
    region: '',
    city: '',
    street: '',
    house: 0,
    flat: 0,
    registrationDate: '',
  },
  homeAddress: {
    country: '',
    region: '',
    city: '',
    street: '',
    house: 0,
    flat: 0,
    registrationDate: '',
  },
  socials: {
    vk: '',
    instagram: '',
    whatsApp: '',
    youTube: '',
    ok: '',
    facebook: '',
    viber: '',
    twitter: '',
    vimeo: '',
    skype: '',
  },
}

export const dataSlice = createSlice({
  name: 'data',
  initialState,
  reducers: {
    updateGeneral: (state, action: PayloadAction<{ field: string; value: string }>) => {
      const { field, value } = action.payload;
      state.general = {
        ...state.general,
        [field]: value,
      };
    },
    updateOwnerShipIp: (state, action: PayloadAction<{ field: string; value: string | number | File }>) => {
      const { field, value } = action.payload;
      state.ownerShipIp = {
        ...state.ownerShipIp,
        [field]: value,
      };
    },
    updateOwnerShipLlc: (state, action: PayloadAction<{ field: string; value: string | number }>) => {
      const { field, value } = action.payload;
      state.ownerShipLlc = {
        ...state.ownerShipLlc,
        [field]: value,
      };
    },
    updateRegistrationAddress: (state, action: PayloadAction<{ field: string; value: string }>) => {
      const { field, value } = action.payload;
      state.registrationAddress = {
        ...state.registrationAddress,
        [field]: value,
      };
    },
    updateHomeAddress: (state, action: PayloadAction<{ field: string; value: string }>) => {
      const { field, value } = action.payload;
      state.homeAddress = {
        ...state.homeAddress,
        [field]: value,
      };
    },
    updateSocials: (state, action: PayloadAction<{ field: string; value: string }>) => {
      const { field, value } = action.payload;
      state.socials = {
        ...state.socials,
        [field]: value,
      };
    },
    resetOwnershipIp: (state) => {
      state.ownerShipIp = initialState.ownerShipIp
    },
    resetOwnershipLlc: (state) => {
      state.ownerShipLlc = initialState.ownerShipLlc
    },
    resetData: () => initialState,
  }
})

export const {
  updateGeneral,
  updateOwnerShipIp,
  updateOwnerShipLlc,
  updateRegistrationAddress,
  updateHomeAddress,
  updateSocials,
  resetOwnershipIp,
  resetOwnershipLlc,
  resetData,
} = dataSlice.actions;

export const selectSubmitJSON = (state: RootState) => state.data;
export const selectGeneral = (state: RootState) => state.data.general;
export const selectOwnershipIP = (state: RootState) => state.data.ownerShipIp;
export const selectOwnershipLlc = (state: RootState) => state.data.ownerShipLlc;
export const selectRegistrationAddress = (state: RootState) => state.data.registrationAddress;
export const selectHomeAddress = (state: RootState) => state.data.homeAddress;
export const selectSocials = (state: RootState) => state.data.socials;

export default dataSlice.reducer;
