import { ProgressItem } from '../progressItem';
import styles from './styles.module.scss';

const steps = [
  {
    step: 1,
    text: 'Общие'
  },
  {
    step: 2,
    text: 'Форма собственности'
  },
  {
    step: 3,
    text: 'Адрес регистрации'
  },
  {
    step: 4,
    text: 'Адрес проживания'
  },
  {
    step: 5,
    text: 'Социальные сети'
  },
]

export const ProgressFill = () => {
  return (
    <section className={styles.progress}>
      <h2 className={styles.progress__title}>
        Создание аккаунта
      </h2>
      <p className={styles.progress__subtitle}>
        Заполните все пункты данной формы и нажмите кнопку «Сохранить».
      </p>
      <div className={styles.progress__items}>
        {steps.map((step, index) => (
          <ProgressItem key={index} step={step.step} text={step.text} />
        ))}
        <div className={styles.progress__line}></div>
      </div>
    </section>
  )
}